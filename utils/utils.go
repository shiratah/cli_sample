package utils

import (
	"io/ioutil"
	"net/http"
	"time"
)

type ErrMakeClient int
type ErrRequest int
type ErrConvertResponse int

func GetRequestBody(url string) (string, error) {
	// 10秒でタイムアウト
	client := &http.Client{Timeout: 10 * time.Second}
	// リクエストを作る
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	// urlに対してGETを実行
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	//クライアントを閉じる
	defer resp.Body.Close()

	// レスポンスボディをバイトコードとして取り出す
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	//バイトコードを文字列に変換
	s := string(body)

	return s, nil
}
